package com.emids.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;


@Controller
public class HIController {

	static{
		System.out.println("hi");
	}
	@RequestMapping(value = "/callinsurance", method = RequestMethod.POST)
	public ModelAndView calInsurance(HttpServletRequest request, @RequestParam("curnthealth") String[] curnthealth,HttpServletResponse response) throws IOException {
		String name = request.getParameter("name");
		String gender = request.getParameter("gender");
		Integer age = Integer.parseInt(request.getParameter("age").trim());
		String[] habitsValue = request.getParameterValues("habits");
		PrintWriter pw=response.getWriter();//get the stream to write the data 
		
		boolean exerciseFlag = false;
		for (int i = 0; i < habitsValue.length; i++) {
			if (habitsValue[i].equals("exercise")) {
				exerciseFlag = true;
			}
		}
		int habites = 0;
		if (exerciseFlag) {
			habites = (habitsValue.length * 3) - 3 * 2;
		} else {
			habites = habitsValue.length * 3;
		}

		int result = 5000;

		// below the age of 18 years
		if (age < 18) {

			// For male
			if (gender.equals("male")) {
				result = result + (result * 2 / 100);

				if (curnthealth.length > 0) {
					result = result + (result * (curnthealth.length) / 100);
					System.out.println("Pre-existing conditions test 1 --->" + (result + (result * habites / 100)));
				} else {
					System.out.println("test 1 :" + age + "without precondition 1---->" + result);
				}
			}

			// For female
			else {
				result = result + 0;
				if (curnthealth.length > 0) {
					result = result + (result * (curnthealth.length) / 100);
					System.out.println("Pre-existing conditions test 2 --->" + (result + (result * habites / 100)));
				} else {
					System.out.println("test 1 :" + age + "without precondition 2---->" + result);
				}
			}
		}

		// between 18 to 25
		else if (age >= 18 && age < 25) {
			result = result + (result / 10);
			System.out.println("test 2---->" + age + "result f---->" + result);
		}

		// between 25 to 30
		else if (age >= 25 && age < 30) {

			if (gender == "male") {
				result = result + (result / 10);
				result = result + (result / 10);

				result = result + (result / 2);
				System.out.println("test  3:" + age + "result m---->" + result);
			} else {
				result = result + (result / 10);
				result = result + (result / 10);
				System.out.println("test 3 :" + age + "result f---->" + result);
			}

		}
		// between 30 to 35
		else if (age >= 30 && age < 35) {
			if (gender.equals("male")) {
				result = result + (result / 10);
				result = result + (result / 10);
				result = result + (result / 10);
				result = result + (result * 2 / 100);
				// Pre-existing conditions for male
				if (curnthealth.length > 0) {
					result = result + (result * (curnthealth.length) / 100);
					System.out.println("Pre-existing conditions test --->" + (result + (result * habites / 100)));
					//writing html in the stream  
					pw.println("Health Insurance Premium for Mr. Gomes:"+result);
				}
			} else {
				result = result + (result / 10);
				result = result + (result / 10);
				result = result + (result / 10);
				System.out.println("test 4 :" + age + "result f---->" + (result + (result * habites / 100)));
			}
		}
		// between 35 to 40
		else if (age >= 35 && age < 40) {
			result = result + (result / 10);
			result = result + (result / 10);
			result = result + (result / 10);
			result = result + (result / 10);
			System.out.println("test 5:" + age + "result f---->" + result);
		}

		// between 40 to 45
		else if (age >= 40 && age < 45) {
			result = result + (result / 10);
			result = result + (result / 10);
			result = result + (result / 10);
			result = result + (result / 10);

			result = result + (result * 20 / 100);
			System.out.println("test 6:" + age + "result f---->" + result);
		}

		return new ModelAndView("result");
	}

}
